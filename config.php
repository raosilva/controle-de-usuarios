<?php

$dns = "mysql:dbname=user_control;host=localhost";
$dbuser = "admin";
$dbpass = "";

try {
    $pdo = new PDO($dns, $dbuser, $dbpass);
} catch (PDOException $e) {
    echo "Falhou a conexão: ".$e->getMessage();
}
