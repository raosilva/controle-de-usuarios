<?php
require 'config.php';
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Controle de Usuários</title>
</head>

<body>
    <table border="0" width="100%">

        <tr>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Ações</th>
        </tr>

        <?php
        $sql = "SELECT * FROM users";
        $sql = $pdo->query($sql);
        if ($sql->rowCount() > 0) {
            foreach ($sql->fetchAll() as $user) {
                echo '<tr>';
                echo '<td>' . $user['nome'] . '</td>';
                echo '<td>' . $user['email'] . '</td>';
                echo '<td><a href = "edit.php?id=' . $user['id'] . '">Editar</a> | <a href = "delete.php?id=' . $user['id'] . '">Excluir</a></td>';
                echo '</tr>';
            }
        }
        ?>

    </table><br>

    <a href="add.php">Adicionar Novo Usuário</a>
</body>

</html>